package pw.jdmarble.joystickmayor.doclet;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Locale;
import javax.tools.DocumentationTool;
import javax.tools.ToolProvider;
import org.junit.jupiter.api.Test;

public class SelfTest {
    @Test
    public void run_javadoc_on_self() throws IOException {
        final var fileManager = ToolProvider.getSystemJavaCompiler().
            getStandardFileManager(null, Locale.getDefault(), Charset.defaultCharset());
        fileManager.setLocation(DocumentationTool.Location.DOCUMENTATION_OUTPUT,
            List.of(new File("./target")));

        assert ToolProvider.getSystemDocumentationTool().getTask(
            null,
            fileManager,
            null,
            JmDoclet.class,
            List.of(
                "-d","target/api",
                "--template-path=src/test/templates/",
                "--source-path=src/test/java/",
                "pw.jdmarble.joystickmayor.doclet"),
            null).call();
    }
}
