package pw.jdmarble.joystickmayor.doclet;

import java.io.File;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import jdk.javadoc.doclet.Doclet;

/**
 * Option for the destination directory for output files.
 */
class OutputPathOption implements Doclet.Option {

    private final Consumer<File> optionSetter;

    OutputPathOption(final Consumer<File> optionSetter) {
        this.optionSetter = optionSetter;
    }

    @Override
    public int getArgumentCount() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "Destination directory for output files";
    }

    @Override
    public Kind getKind() {
        return Kind.STANDARD;
    }

    @Override
    public List<String> getNames() {
        return List.of(
            "-d"
        );
    }

    @Override
    public String getParameters() {
        return "directory";
    }

    @Override
    public boolean process(final String opt, final List<String> arguments) {
        optionSetter.accept(new File(arguments.get(0)));
        return true;
    }
}
