package pw.jdmarble.joystickmayor.doclet;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.function.Function;
import javax.lang.model.SourceVersion;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.PackageElement;
import javax.tools.Diagnostic;
import javax.tools.DocumentationTool;
import javax.tools.FileObject;
import javax.tools.StandardJavaFileManager;
import jdk.javadoc.doclet.Doclet;
import jdk.javadoc.doclet.DocletEnvironment;
import jdk.javadoc.doclet.Reporter;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import com.vladsch.flexmark.html.HtmlRenderer;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.options.MutableDataSet;

/**
 * The `Doclet` to use for the documentation tool.
 */
public class JmDoclet implements Doclet {

    private final VelocityEngine velocityEngine = new VelocityEngine();
    private Reporter reporter;
    private File outputLocation;
    private Parser mdParser;
    private HtmlRenderer htmlRenderer;

    @Override
    public void init(final Locale locale, final Reporter reporter) {
        this.reporter = reporter;

        final var options = new MutableDataSet();
        mdParser = Parser.builder(options).build();
        htmlRenderer = HtmlRenderer.builder(options).build();
    }

    @Override
    public String getName() {
        // The name of the class without the "Doclet".
        return getClass().getSimpleName().replace(Doclet.class.getSimpleName(), "");
    }

    @Override
    public Set<? extends Option> getSupportedOptions() {
        return Set.of(
            new TemplatePathOption(velocityEngine),
            new OutputPathOption(path -> outputLocation = path)
        );
    }

    @Override
    public SourceVersion getSupportedSourceVersion() {
        return SourceVersion.RELEASE_11;
    }

    @Override
    public boolean run(final DocletEnvironment docletEnvironment) {
        try {
            if (!outputLocation.exists() && !outputLocation.mkdirs()) {
                throw new IOException("Could not create directory " + outputLocation);
            }
            // This cast is not nice, but it is what the StandardDoclet does (see StandardDocFileFactory).
            // So, I guess it is the only way.
            ((StandardJavaFileManager) docletEnvironment.getJavaFileManager()).
                setLocation(DocumentationTool.Location.DOCUMENTATION_OUTPUT, List.of(outputLocation));
        } catch (final IOException e) {
            throw new RuntimeException(
                "Could not set " + DocumentationTool.Location.DOCUMENTATION_OUTPUT + " to " + outputLocation, e);
        }
        velocityEngine.init();

        for (final Element element : docletEnvironment.getIncludedElements()) {
            final var context = new VelocityContext();
            context.put("element", element);
            context.put("docTrees", docletEnvironment.getDocTrees());
            context.put("toMd", (Function<String, String>) this::renderMdToHtml);
            final var template = velocityEngine.getTemplate(element.getKind().name() + ".html.vm");

            final var packageName = getPackageName(element);
            final FileObject fileObject;
            try {
                final String elementName = element.getSimpleName().toString();
                if (!elementName.isBlank()) {
                    fileObject = docletEnvironment.getJavaFileManager().getFileForOutput(
                        DocumentationTool.Location.DOCUMENTATION_OUTPUT,
                        packageName.toString(),
                        elementName + ".html",
                        null);
                    try (final var writer = fileObject.openWriter()) {
                        template.merge(context, writer);
                    }
                }
            } catch (final IOException e) {
                reporter.print(Diagnostic.Kind.ERROR, element, e.getMessage());
            }
        }
        return true;
    }

    private CharSequence getPackageName(final Element element) {
        final CharSequence packageName;
        if (element == null) {
            packageName = "";
        } else if (element.getKind() == ElementKind.PACKAGE) {
            packageName = ((PackageElement) element).getQualifiedName();
        } else {
            packageName = getPackageName(element.getEnclosingElement());
        }
        return packageName;
    }

    private String renderMdToHtml(final String input) {
        if (input == null) {
            return null;
        }
        return htmlRenderer.render(mdParser.parse(input));
    }
}
