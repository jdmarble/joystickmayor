package pw.jdmarble.joystickmayor.doclet;

import java.util.List;
import jdk.javadoc.doclet.Doclet;
import org.apache.velocity.app.VelocityEngine;

/**
 * Option for the path to templates.
 */
class TemplatePathOption implements Doclet.Option {

    private final VelocityEngine velocityEngine;

    TemplatePathOption(final VelocityEngine velocityEngine) {
        this.velocityEngine = velocityEngine;
    }

    @Override
    public int getArgumentCount() {
        return 1;
    }

    @Override
    public String getDescription() {
        return "path to templates";
    }

    @Override
    public Kind getKind() {
        return Kind.STANDARD;
    }

    @Override
    public List<String> getNames() {
        return List.of(
            "--template-path"
        );
    }

    @Override
    public String getParameters() {
        return "path";
    }

    @Override
    public boolean process(final String opt, final List<String> arguments) {
        velocityEngine.setProperty("file.resource.loader.path", arguments.get(0));
        return true;
    }
}
