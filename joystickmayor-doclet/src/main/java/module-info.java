/**
 * Contains the basic Doclet implementation.
 */
module pw.jdmarble.joystickmayor.doclet {
    requires jdk.javadoc;
    requires flexmark;
    requires velocity.engine.core;
    requires flexmark.util;
    exports pw.jdmarble.joystickmayor.doclet;
}
